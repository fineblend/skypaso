(function(angular, undefined) {
'use strict';

angular.module('skypasoApp.constants', [])

.constant('appConfig', {userRoles:['guest','user','admin']})

;
})(angular);