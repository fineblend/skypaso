'use strict';

angular.module('skypasoApp.admin', [
  'skypasoApp.auth',
  'ui.router'
]);
