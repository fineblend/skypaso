'use strict';

angular.module('skypasoApp')
  .config(function ($stateProvider) {
    $stateProvider
      .state('price', {
        url: '/price',
        templateUrl: 'app/price/price.html',
        controller: 'PriceCtrl'
      });
  });
