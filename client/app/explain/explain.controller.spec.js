'use strict';

describe('Controller: ExplainCtrl', function () {

  // load the controller's module
  beforeEach(module('skypasoApp'));

  var ExplainCtrl, scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    ExplainCtrl = $controller('ExplainCtrl', {
      $scope: scope
    });
  }));

  it('should ...', function () {
    expect(1).to.equal(1);
  });
});
