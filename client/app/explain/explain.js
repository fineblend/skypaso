'use strict';

angular.module('skypasoApp')
  .config(function ($stateProvider) {
    $stateProvider
      .state('explain', {
        url: '/explain',
        templateUrl: 'app/explain/explain.html',
        controller: 'ExplainCtrl'
      });
  });
