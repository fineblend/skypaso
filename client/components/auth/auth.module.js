'use strict';

angular.module('skypasoApp.auth', [
  'skypasoApp.constants',
  'skypasoApp.util',
  'ngCookies',
  'ui.router'
])
  .config(function($httpProvider) {
    $httpProvider.interceptors.push('authInterceptor');
  });
